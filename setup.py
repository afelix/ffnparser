# encoding=utf-8
from setuptools import setup, find_packages
import os

version = {}
with open(os.path.join('ffnparser', 'version.py')) as fp:
    exec(fp.read(), version)


setup(
    name='ffnparser',
    version=version['__version__'],
    description='Simple parser that reads fanfics from fanfiction.net and outputs them in memory in different formats',
    author='Arlena Derksen',
    author_email='arlena@hubsec.eu',
    license='MIT',
    classifiers=[
            'Development Status :: 4 - Beta',
            'Intended Audience :: Developers',
            'Programming Language :: Python :: 3.6'
    ],
    packages=find_packages(),
    install_requires=['reportlab', 'requests', 'beautifulsoup4', 'cached_property', 'lxml', 'html5lib', 'raven']
)
