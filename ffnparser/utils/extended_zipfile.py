# encoding=utf-8
import time

from zipfile import ZipFile, ZipInfo, ZIP_STORED
from io import StringIO, BytesIO


class ExtendedZipFile(ZipFile):
    def __init__(self, file, mode="r", compression=ZIP_STORED, allowZip64=True):
        super().__init__(file, mode, compression, allowZip64)

    def writeio(self, zinfo_or_arcname, data, compress_type=None):
        """Write a file into the archive.  The contents is 'data', which
        may be either a 'stringIO' or a 'bytesIO' instance; if it is a
        'stringIO', it is read and encoded as UTF-8 first.
        'zinfo_or_arcname' is either a ZipInfo instance or
        the name of the file in the archive."""
        if isinstance(data, StringIO):
            data = data.getvalue().encode('utf-8')
        elif isinstance(data, BytesIO):
            data = data.getvalue()
        else:
            raise ValueError(
                "Data of type {0} is not implemented. Only use this function when writing streams".format(
                    type(data)
                )
            )

        if not isinstance(zinfo_or_arcname, ZipInfo):
            zinfo = ZipInfo(filename=zinfo_or_arcname,
                            date_time=time.localtime(time.time())[:6])
            zinfo.compress_type = self.compression
            if zinfo.filename[-1] == '/':
                zinfo.external_attr = 0o40775 << 16  # drwxrwxr-x
                zinfo.external_attr |= 0x10  # MS-DOS directory flag
            else:
                zinfo.external_attr = 0o600 << 16  # ?rw-------
        else:
            zinfo = zinfo_or_arcname

        if not self.fp:
            raise ValueError(
                "Attempt to write to ZIP archive that was already closed")
        if self._writing:
            raise ValueError(
                "Can't write to ZIP archive while an open writing handle exists."
            )

        if compress_type is not None:
            zinfo.compress_type = compress_type

        zinfo.file_size = len(data)  # Uncompressed size
        with self._lock:
            with self.open(zinfo, mode='w') as dest:
                dest.write(data)
