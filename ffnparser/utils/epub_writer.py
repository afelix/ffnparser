# coding=utf-8
import lxml.etree as ET
import uuid
from datetime import datetime
import zipfile
import os

from .extended_zipfile import ExtendedZipFile

__author__ = 'Arlena <arlena@hubsec.eu>'

_META_INF_PATH = 'META-INF'
_BOOK_CONTAINER_PATH = 'OEBPS'
_FFNPARSER_NAMESPACE_UUID = uuid.UUID('1c56abc9-8377-4bd7-9f20-a9e86f3e70b7')
NAMESPACES = {
    'xmlns': 'http://www.w3.org/1999/xhtml',
    'xmlns:epub': 'http://www.idpf.org/2007/ops',
    'xmlns:opf': 'http://www.idpf.org/2007/opf',
    'xmlns:dc': 'http://purl.org/dc/elements/1.1/',
    'xmlns:xml': 'http://www.w3.org/XML/1998/namespace',
    'container_ns': 'urn:oasis:names:tc:opendocument:xmlns:container'
}


class EpubWriter:
    def __init__(self, fp, book_meta):
        self._chapters = {}
        self._book_meta = book_meta

        self._out_file = ExtendedZipFile(fp, 'w', zipfile.ZIP_STORED)

        self._out_file.writestr('mimetype', 'application/epub+zip', compress_type=zipfile.ZIP_STORED)

        self._write_container()
        self._write_cover()

    def _write_container(self):
        container = ET.Element(
            'container',
            attrib={
                'version': '1.0',
            },
            nsmap={
                None: NAMESPACES['container_ns']
            }
        )

        root_files = ET.SubElement(container, 'rootfiles')

        ET.SubElement(root_files, 'rootfile', attrib={
            'full-path': os.path.join(_BOOK_CONTAINER_PATH, 'content.opf'),
            'media-type': 'application/oebps-package+xml'
        })

        self._out_file.writestr(
            os.path.join(_META_INF_PATH, 'container.xml'),
            ET.tostring(container, encoding='utf-8', pretty_print=True, xml_declaration=True)
        )

    def _write_content_opf(self):
        creation_timestamp = datetime.utcnow()

        package = ET.Element(
            'package',
            attrib={
                'version': '3.0',
                '{{{0}}}lang'.format(NAMESPACES['xmlns:xml']): 'en',
                'unique-identifier': 'pub-id'
            },
            nsmap={
                None: NAMESPACES['xmlns:opf'],
                'xml': NAMESPACES['xmlns:xml']
            }
        )

        metadata = ET.SubElement(package, 'metadata', nsmap={
            'dc': NAMESPACES['xmlns:dc']
        })

        ET.SubElement(metadata, '{{{0}}}identifier'.format(NAMESPACES['xmlns:dc']), attrib={
            'id': 'pub-id'
        }).text = uuid.uuid5(
            _FFNPARSER_NAMESPACE_UUID,
            '{0}_{1}'.format(self._book_meta.author, self._book_meta.title)
        ).urn

        ET.SubElement(metadata, 'meta', attrib={
            'refines': '#pub-id',
            'property': 'identifier-type',
            'scheme': 'xsd:string'
        }).text = 'uuid'

        ET.SubElement(
            metadata,
            '{{{0}}}language'.format(NAMESPACES['xmlns:dc'])
        ).text = self._book_meta.language
        ET.SubElement(
            metadata,
            '{{{0}}}title'.format(NAMESPACES['xmlns:dc'])
        ).text = self._book_meta.title

        ET.SubElement(
            metadata,
            '{{{0}}}creator'.format(NAMESPACES['xmlns:dc']),
            attrib={
                'id': 'creator'
            }
        ).text = self._book_meta.author

        ET.SubElement(metadata, 'meta', attrib={
            'refines': '#creator',
            'property': 'role',
            'scheme': 'marc:relators'
        }).text = 'aut'

        ET.SubElement(metadata, 'meta', attrib={
            'property': 'dcterms:modified'
        }).text = creation_timestamp.strftime('%Y-%m-%dT%H:%M:%SZ')

        ET.SubElement(
            metadata,
            '{{{0}}}publisher'.format(NAMESPACES['xmlns:dc'])
        ).text = self._book_meta.publisher
        ET.SubElement(
            metadata,
            '{{{0}}}date'.format(NAMESPACES['xmlns:dc'])
        ).text = creation_timestamp.strftime('%Y-%m-%dT%H:%M:%SZ')
        ET.SubElement(
            metadata,
            'meta',
            attrib={
                'property': 'dcterms:dateCopyrighted'
            }
        ).text = self._book_meta.copyright_timestamp.strftime('%Y-%m-%d')

        manifest = ET.SubElement(package, 'manifest')
        spine = ET.SubElement(package, 'spine')

        ET.SubElement(manifest, 'item', attrib={
            'id': 'nav',
            'href': 'nav.xhtml',
            'media-type': 'application/xhtml+xml',
            'properties': 'nav'
        })

        ET.SubElement(manifest, 'item', attrib={
            'id': 'cover',
            'href': 'cover.xhtml',
            'media-type': 'application/xhtml+xml'
        })

        if self._book_meta.cover_img_fp is not None:
            ET.SubElement(manifest, 'item', attrib={
                'id': 'cover-img',
                'href': 'cover.jpg',
                'media-type': 'image/jpeg',
                'properties': 'cover-image'
            })

        ET.SubElement(spine, 'itemref', attrib={
            'idref': 'cover',
            'linear': 'no'
        })

        ET.SubElement(spine, 'itemref', attrib={
            'idref': 'nav'
        })

        for n_chapter in self._chapters.keys():
            ET.SubElement(manifest, 'item', attrib={
                'id': 'chapter-{0}'.format(n_chapter),
                'href': 'chapter-{0}.xhtml'.format(n_chapter),
                'media-type': 'application/xhtml+xml'
            })

            ET.SubElement(spine, 'itemref', attrib={
                'idref': 'chapter-{0}'.format(n_chapter)
            })

        self._out_file.writestr(
            os.path.join(_BOOK_CONTAINER_PATH, 'content.opf'),
            ET.tostring(package, encoding='utf-8', pretty_print=True, xml_declaration=True)
        )

    # TODO: Continue here

    def _write_nav(self):
        html = ET.Element('html', nsmap={
            None: NAMESPACES['xmlns'],
            'epub': NAMESPACES['xmlns:epub']
        })

        head = ET.SubElement(html, 'head')
        ET.SubElement(head, 'title').text = 'Table of contents'

        body = ET.SubElement(html, 'body')

        nav_toc = ET.SubElement(
            body,
            'nav',
            attrib={
                '{{{0}}}type'.format(NAMESPACES['xmlns:epub']): 'toc',
                'id': 'toc'
            },
            nsmap={
                'epub': NAMESPACES['xmlns:epub']
            }
        )

        ET.SubElement(nav_toc, 'h1').text = 'Table of contents'
        outer_ol = ET.SubElement(nav_toc, 'ol')

        for n_chapter, chapter_info in self._chapters.items():
            li = ET.SubElement(outer_ol, 'li')
            a = ET.SubElement(li, 'a', attrib={
                'href': 'chapter-{0}.xhtml'.format(n_chapter)
            }).text = chapter_info['title']

        self._out_file.writestr(
            os.path.join(_BOOK_CONTAINER_PATH, 'nav.xhtml'),
            ET.tostring(html, encoding='utf-8', pretty_print=True, xml_declaration=True, method='xml')
        )

    def write_chapter(self, n_chapter, title, chapter_tree):
        self._chapters[n_chapter] = {'title': title}

        html = ET.Element('html', nsmap={
            None: NAMESPACES['xmlns'],
            'epub': NAMESPACES['xmlns:epub']
        })

        head = ET.SubElement(html, 'head')
        ET.SubElement(head, 'title').text = self._chapters[n_chapter]['title']

        body = ET.SubElement(html, 'body')

        body.append(chapter_tree)

        self._out_file.writestr(
            os.path.join(_BOOK_CONTAINER_PATH, 'chapter-{0}.xhtml'.format(n_chapter)),
            ET.tostring(html, encoding='utf-8', pretty_print=True, xml_declaration=True, method='xml')
        )

    def _write_cover(self):
        html = ET.Element('html', nsmap={
            None: NAMESPACES['xmlns'],
            'epub': NAMESPACES['xmlns:epub']
        })

        head = ET.SubElement(html, 'head')
        ET.SubElement(head, 'title').text = self._book_meta.title

        body = ET.SubElement(html, 'body')
        ET.SubElement(body, 'h1').text = self._book_meta.title
        ET.SubElement(body, 'br').text = None
        ET.SubElement(body, 'h2').text = u'by {0}'.format(self._book_meta.author)

        if self._book_meta.cover_img_fp is not None:
            for _ in range(2):
                ET.SubElement(body, 'br').text = None
            ET.SubElement(body, 'img', attrib={
                'src': 'cover.jpg',
                'alt': 'Cover image'
            })
            self._out_file.writeio(
                os.path.join(_BOOK_CONTAINER_PATH, 'cover.jpg'),
                self._book_meta.cover_img_fp
            )

        self._out_file.writestr(
            os.path.join(_BOOK_CONTAINER_PATH, 'cover.xhtml'),
            ET.tostring(html, encoding='utf-8', pretty_print=True, xml_declaration=True, method='xml')
        )

    def finish(self):
        self._write_nav()
        self._write_content_opf()


class BookMeta(object):
    def __init__(self, author, title, publisher, language, copyright_timestamp, cover_img_fp):
        self.author = author
        self.title = title
        self.publisher = publisher
        self.language = language
        self.copyright_timestamp = copyright_timestamp
        self.cover_img_fp = cover_img_fp
