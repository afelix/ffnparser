# coding=utf-8
from __future__ import unicode_literals, print_function, absolute_import

from datetime import datetime
import time
from io import BytesIO

import requests
from bs4 import BeautifulSoup
from cached_property import cached_property

from .exceptions import FicNotFoundException, CoverNotFoundException
from .handlers import Handler, HandlerPDF

REQUEST_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Accept': ('text/html, application/xml;q=0.9, application/xhtml xml, image/png, image/jpeg, image/gif, '
               'image/x-xbitmap, */*;q=0.1'),
    'Accept-Language': 'en',
    'Accept-Charset': 'iso-8859-1, utf-8, utf-16, *;q=0.1',
    'Accept-Encoding': 'deflate, gzip, x-gzip, identity, *;q=0',
    'Connection': 'Keep-Alive, TE',
    'TE': 'deflate, gzip, chunked, identity, trailers'
}


class Parser(object):
    def __init__(self, fic_id, handlers=None):
        self._cover_fp = BytesIO()

        self._fic_id = fic_id

        self._session = requests.Session()
        self._session.headers = REQUEST_HEADERS
        self._session.headers.update({'Referer': self.url})

        try:
            self._download_cover_image()
        except CoverNotFoundException:
            self._cover_path = None

        if not handlers or not isinstance(handlers, list):
            handlers = [
                Handler(handler=HandlerPDF(), fp=BytesIO())
            ]

        self.handlers = handlers

        for handler in self.handlers:
            if not isinstance(handler, Handler):
                raise ValueError(
                    "Handlers should be a list of instances of `ffnparser.handlers.Handler`; handler seen is `{0}`"
                    .format(type(handler))
                )

            handler.handler.start(handler.fp, self._meta, self._cover_fp)

    @property
    def url(self):
        return 'https://www.fanfiction.net/s/{0}'.format(self._fic_id)

    @cached_property
    def _meta(self):
        fic_meta = dict()
        soup = self._get_soup(self.url)

        div_info = soup.find(id='profile_top')
        fic_meta['title'] = div_info.find_all('b', class_='xcontrast_txt')[0].contents[0]
        fic_meta['author'] = div_info.find_all('a', class_='xcontrast_txt')[0].text

        # number of chapters
        txt = div_info.find_all('span', class_='xgray xcontrast_txt')[-1].text
        chapters_start = txt.find('Chapters: ') + len('Chapters: ')
        chapters_end = txt.find('- Words: ', chapters_start)

        try:
            fic_meta['n_chapters'] = int(txt[chapters_start:chapters_end].strip())
        except ValueError:  # Oneshot
            fic_meta['n_chapters'] = 1

        words_start = chapters_end + len('- Words: ')
        words_end = txt.find(' - ', words_start)
        fic_meta['n_words'] = int(txt[words_start:words_end].replace(',', ''))

        publish_info = div_info.find_all('span')[-1]
        try:
            fic_meta['publication_timestamp'] = datetime.utcfromtimestamp(int(publish_info.attrs['data-xutime']))
        except ValueError:
            # invalid publication date
            fic_meta['publication_timestamp'] = datetime.utcfromtimestamp(0)

        # fic cover url
        cover_info = soup.find(id='img_large')
        url = cover_info.find_all('img')[0].get('data-original', None)
        if url:
            fic_meta['cover_url'] = u'https:{0}'.format(url)
        else:
            fic_meta['cover_url'] = None

        return fic_meta

    def _get_soup(self, url):
        r = self._session.get(url)

        if r.status_code == 503:
            time.sleep(5)
            r = self._session.get(url)

        r.raise_for_status()

        if '<span class=\'gui_warning\'>Story Not Found' in r.text:
            raise FicNotFoundException(url)

        return BeautifulSoup(r.text, 'html5lib')

    def _download_cover_image(self):
        if len(self._cover_fp.getvalue()) > 0:
            # Already a cover image present, give a runtime warning and download again
            # TODO
            self._cover_fp = BytesIO()

        if not self._meta['cover_url']:
            raise CoverNotFoundException('No cover image found for fic {0!r}'.format(self._fic_id))

        r = self._session.get(self._meta['cover_url'], stream=True)

        if r.status_code == 503:
            time.sleep(5)
            r = self._session.get(self._meta['cover_url'], stream=True)

        r.raise_for_status()

        for chunk in r.iter_content():
            self._cover_fp.write(chunk)

    def _cleanup(self):
        self._cover_fp.close()

    def _get_chapter_title(self, n_chapter, chapter_soup):
        chapter_select = chapter_soup.find(id='chap_select')
        try:
            return chapter_select.find('option', value=n_chapter).text
        except AttributeError:
            # Oneshot
            return self._meta['title']

    def parse(self):
        for n_chapter in range(1, self._meta['n_chapters'] + 1):
            chapter_url = '{0}/{1}/'.format(self.url, n_chapter)
            chapter_soup = self._get_soup(chapter_url)
            chapter_title = self._get_chapter_title(n_chapter, chapter_soup)
            for handler in self.handlers:
                handler.handler.parse_chapter(n_chapter, chapter_title, chapter_soup)

        for handler in self.handlers:
            handler.handler.finish()

        self._cleanup()
