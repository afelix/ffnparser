# encoding=utf8
from __future__ import absolute_import, unicode_literals, print_function

from .base import BaseHandler


class HandlerText(BaseHandler):
    def start(self, fp, meta, cover_fp):
        raise NotImplementedError

    def parse_chapter(self, n_chapter, title, soup):
        # content_div = soup.find(id='storytextp').contents
        # paragraphs = []
        # for paragraph in content_div:
        #     print(type(paragraph), paragraph)

        raise NotImplementedError

    def finish(self):
        raise NotImplementedError
