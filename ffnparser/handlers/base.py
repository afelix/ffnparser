# encoding=utf8
from __future__ import absolute_import, unicode_literals
import abc


class BaseHandler(object):
    __metaclass__ = abc.ABCMeta

    fp = None
    meta = None
    cover_fp = None

    def start(self, fp, meta, cover_fp):
        self.fp = fp
        self.meta = meta
        self.cover_fp = cover_fp

    def finish(self):
        raise NotImplementedError

    def parse_chapter(self, n_chapter, title, soup):
        raise NotImplementedError
