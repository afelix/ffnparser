# encoding=utf8
from __future__ import absolute_import, unicode_literals

import lxml.etree as ET
import html5lib

from .base import BaseHandler
from ..utils.epub_writer import EpubWriter, BookMeta, NAMESPACES


class HandlerEpub(BaseHandler):
    _writer = None

    def start(self, fp, meta, cover_fp):
        super(HandlerEpub, self).start(fp, meta, cover_fp)

        self._writer = EpubWriter(fp, BookMeta(
            meta['author'],
            meta['title'],
            'fanfiction.net',
            'en',
            meta['publication_timestamp'],
            cover_fp
        ))

    def parse_chapter(self, n_chapter, title, soup):
        content_div = ET.Element('div', attrib={'id': 'content'})
        ET.SubElement(content_div, 'h1').text = title
        fic_tree = html5lib.parseFragment(str(soup.find(id='storytext')), treebuilder='lxml')

        hr_list = fic_tree[0].findall('{{{0}}}hr'.format(NAMESPACES['xmlns']))
        for hr in hr_list:
            del hr.attrib['noshade']

        content_div.append(fic_tree[0])

        self._writer.write_chapter(n_chapter, title, content_div)

    def finish(self):
        self._writer.finish()
