# encoding=utf8
from __future__ import absolute_import, unicode_literals, print_function, division

from bs4 import NavigableString, element, BeautifulSoup

from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.platypus import Paragraph, PageBreak, Flowable, SimpleDocTemplate, Image
from reportlab.platypus.tableofcontents import TableOfContents
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch

from .base import BaseHandler


class HandlerPDF(BaseHandler):
    doc = None

    def __init__(self):
        self._elements = []
        base_style = getSampleStyleSheet()

        self.style = base_style['BodyText']

        self.h1 = base_style['Heading1']
        self.h2 = base_style['Heading2']
        self.title_style = base_style['Title']

        # Sentry
        from raven import Client
        from ..version import __version__
        self._sentry_client = Client(
            # Safer than you think it is... will be revoked on abuse
            'https://308c4b07a9464648b7bf9ebe71464953:bafd31e2c0734994bca292786c8a8a82@sentry.io/297447',
            include_paths=['ffnparser'],
            release=__version__
        )

    # PDF methods
    def _add_object_to_document(self, rlt_object):
        self._elements.append(rlt_object)

    def _create_front_page(self, meta, cover_fp):
        self._add_object_to_document(self._generate_paragraph('<br/><br/><br/><br/><br/><br/><br/><br/>'))
        self._add_object_to_document(self._generate_title(u'<b>{0}</b>'.format(meta['title'])))
        self._add_object_to_document(self._generate_paragraph('<br/><br/>'))
        self._add_object_to_document(self._generate_h1(u'by {0}'.format(meta['author'])))
        if cover_fp:
            self._add_object_to_document(self._generate_paragraph('<br/><br/><br/><br/>'))
            self._add_object_to_document(self._generate_image(cover_fp))
        self._add_object_to_document(self._generate_page_break())

    @staticmethod
    def _generate_image(cover_fp):
        return Image(cover_fp)

    def _generate_paragraph(self, formatted_text):
        return Paragraph(formatted_text, self.style)

    def _generate_h1(self, formatted_text):
        return Paragraph(formatted_text, self.h1)

    def _generate_h2(self, formatted_text):
        return Paragraph(formatted_text, self.h2)

    def _generate_title(self, title_text):
        return Paragraph(title_text, self.title_style)

    def _generate_hr(self):
        return HorizontalLineFlowable(self.doc.width)

    @staticmethod
    def _generate_page_break():
        return PageBreak()

    # Parsing methods
    def _r_fix_formatting(self, elem):
        try:
            _ = elem.contents
        except AttributeError:
            if isinstance(elem, str):
                return elem
            else:
                return elem.string
        else:
            for item in elem.contents:
                index = elem.contents.index(item)
                if isinstance(item, element.Tag):
                    # Custom Reportlab tags
                    if item.name == 'em':
                        item.name = 'i'
                    elif item.name == 'strong':
                        item.name = 'b'
                    elif item.name == 'br':
                        item = ''
                    elif item.name == 'span' and (
                            item.get('style') == 'text-decoration: underline;' or
                            item.get('style') == 'text-decoration:underline;'
                    ):
                        del item['style']
                        item.name = 'u'
                    else:
                        # Unimplemented tag found, forward for logging and future implementation
                        self._sentry_client.captureMessage('New tag found', data={
                            'tag': {
                                'name': item.name,
                                'attributes': item.attrs
                            }
                        }, tags={
                            'handler': 'pdf'
                        })
                elem.contents[index] = item
                self._r_fix_formatting(item)
            return elem

    def _fix_formatting_paragraph(self, elem):
        if isinstance(elem[0], str):
            return elem[0]
        else:
            data = []
            for item in elem:
                data.append(str(item))
            soup = BeautifulSoup(u''.join(data), 'lxml')
            new = self._r_fix_formatting(soup.body)
            for item in new.contents:
                return item

    # Implement abstract methods
    def start(self, fp, meta, cover_fp):
        super(HandlerPDF, self).start(fp, meta, cover_fp)

        self.doc = FicTemplate(fp, pagesize=A4, leftMargin=inch, rightMargin=inch, topMargin=inch,
                               bottomMargin=inch, title=meta['title'], author=meta['author'])

        self._create_front_page(meta, cover_fp)

        toc = TableOfContents()
        self._add_object_to_document(self._generate_title('<b>Table of contents</b>'))
        self._add_object_to_document(toc)
        self._add_object_to_document(self._generate_page_break())

    def parse_chapter(self, n_chapter, title, soup):
        self._add_object_to_document(self._generate_h1(title))

        fic = soup.find(id='storytext').contents
        for paragraph in fic[:-1]:
            if paragraph.name == 'hr':
                self._add_object_to_document(self._generate_hr())
            else:
                if not isinstance(paragraph, NavigableString):
                    if len(paragraph.contents) == 0:
                        para = None
                    else:
                        para = self._fix_formatting_paragraph(paragraph.contents)
                        if isinstance(para, element.Tag):
                            para = str(para)
                else:
                    para = paragraph.string
                try:
                    if para is not None:
                        self._add_object_to_document(self._generate_paragraph(para))
                except (TypeError, ValueError):
                    # Unimplemented tag found, forward for logging and future implementation
                    self._sentry_client.captureMessage('New tag found', data={
                        'tag': {
                            'name': para.name,
                            'attributes': para.attrs
                        }
                    }, tags={
                        'handler': 'pdf'
                    })
        self._add_object_to_document(self._generate_page_break())

    def finish(self):
        self.doc.multiBuild(self._elements)


class HorizontalLineFlowable(Flowable):
    def __init__(self, page_width, size=0.75):
        Flowable.__init__(self)
        self.style = getSampleStyleSheet()['BodyText']
        self._page_width = page_width
        if size > 1.0:
            self._size = 1.0
        elif size <= 0:
            self._size = 1.0
        else:
            self._size = size

    def draw(self):
        canvas = self.canv
        canvas.saveState()
        canvas.setStrokeColor(colors.black)
        fudge = self.getSpaceAfter() / 2
        if self._size == 1.0:
            canvas.line(0, fudge, self._page_width, fudge)
        else:
            margin = (self._page_width - (self._page_width * self._size)) / 2
            canvas.line(margin, fudge, self._page_width - margin, fudge)
        canvas.restoreState()


class FicTemplate(SimpleDocTemplate):
    def afterFlowable(self, flowable):
        if flowable.__class__.__name__ == 'Paragraph' and self.page != 1:
            text = flowable.getPlainText()
            style = flowable.style.name
            if style == 'Heading1':
                self.notify('TOCEntry', (0, text, self.page))
            elif style == 'Heading2':
                self.notify('TOCEntry', (1, text, self.page))