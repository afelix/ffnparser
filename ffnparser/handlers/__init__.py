# encoding=utf-8
from collections import namedtuple

from .base import BaseHandler
from .epub import HandlerEpub
from .pdf import HandlerPDF
from .txt import HandlerText


Handler = namedtuple('Handler', ['handler', 'fp'])

__all__ = ('BaseHandler', 'HandlerEpub', 'HandlerText', 'HandlerPDF', 'Handler')
