# encoding=utf8
from __future__ import unicode_literals, absolute_import


class InvalidURLException(Exception):
    def __init__(self, url):
        self.url = url

    def __str__(self):
        return 'The given url is not valid: {0}'.format(self.url)


class FicNotFoundException(Exception):
    def __init__(self, url):
        self.url = url

    def __str__(self):
        return 'The fic at {0} can not be found'.format(self.url)


class CoverNotFoundException(Exception):
    def __init__(self, msg):
        self.message = msg
